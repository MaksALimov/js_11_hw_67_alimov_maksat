const initialState = {
    numbers: '',
    starsCounter: '',
    accessGranted: '',
    accessDenied: '',
    password: 7091,
    correctPassColor: '',
    incorrectPassColor: '',
    onKeyDown: '',
    onKeyDownStars: '',
};

const reducer = (state = initialState, action) => {
    if (action.type === 'number') {
        return {
            ...state,
            numbers: state.numbers.substring(0, 3) + action.value,
            starsCounter: state.starsCounter.substring(0, 3) + '*',
        }
    }

    if (action.type === 'delete') {
        return {
            ...state,
            numbers: state.numbers.slice(0, state.numbers.length - 1),
            starsCounter: state.starsCounter.slice(0, state.starsCounter.length - 1),
            onKeyDown: state.onKeyDown.slice(0, state.onKeyDown.length - 1),
            onKeyDownStars: state.onKeyDownStars.slice(0, state.onKeyDownStars.length - 1),
            accessGranted: state.accessGranted = '',
            accessDenied: state.accessDenied = '',
            correctPassColor: state.correctPassColor = '',
            incorrectPassColor: state.incorrectPassColor = '',
        }
    }

    if (action.type === 'check') {
        if (state.password === +state.numbers || state.password === +state.onKeyDown) {
            return {
                ...state,
                accessGranted: state.accessGranted + 'Access Granted ',
                correctPassColor: state.correctPassColor + 'Green',
            }
        } else {
            return {
                ...state,
                accessDenied: state.accessDenied + 'Access Denied',
                incorrectPassColor: state.incorrectPassColor + 'Red',
            }
        }

    }

    if (action.type === 'keyPress') {
        if (action.value.code === 'Backspace') {
            return {
                ...state,
                onKeyDown: state.onKeyDown.slice(0, state.onKeyDown.length - 1),
                numbers: state.numbers.slice(0, state.numbers.length - 1),
                starsCounter: state.starsCounter.slice(0, state.starsCounter.length - 1),
                onKeyDownStars: state.onKeyDownStars.slice(0, state.onKeyDownStars.length - 1),
                accessGranted: state.accessGranted = '',
                accessDenied: state.accessDenied = '',
                correctPassColor: state.correctPassColor = '',
                incorrectPassColor: state.incorrectPassColor = '',
            }
        }

        if (action.value.code === 'Enter') {
            if (state.password === +state.numbers || state.password === +state.onKeyDown) {
                return {
                    ...state,
                    accessGranted: state.accessGranted + 'Access Granted ',
                    correctPassColor: state.correctPassColor + 'Green',
                }
            } else {
                return {
                    ...state,
                    accessDenied: state.accessDenied + 'Access Denied',
                    incorrectPassColor: state.incorrectPassColor + 'Red',
                }
            }
        }

        return {
            ...state,
            onKeyDown: state.onKeyDown.substring(0, 3) + action.value.key,
            onKeyDownStars: state.onKeyDownStars.substring(0, 3) + '*',
        }
    }

    return state;
};

export default reducer;