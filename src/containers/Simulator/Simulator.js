import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import './Simulator.css';

const Simulator = () => {
    const dispatch = useDispatch();

    const stars = useSelector(state => state.starsCounter);
    const accessGranted = useSelector(state => state.accessGranted);
    const accessDenied = useSelector(state => state.accessDenied);
    const correctPassColor = useSelector(state => state.correctPassColor);
    const incorrectPassColor = useSelector(state => state.incorrectPassColor);
    const keyDownStars = useSelector(state => state.onKeyDownStars);

    const numbers = inputNumber => dispatch({type: 'number', value: inputNumber});
    const deleteLastSymbol = () => dispatch({type: 'delete'});
    const checkPassword = () => dispatch({type: 'check'});


    const classes = ['Screen'];
    classes.push(correctPassColor, incorrectPassColor);

    return (
        <div className="Wrapper">
            <h2>Можно вводить цивры на клавиатуре, удалять и проверять тоже</h2>
            <input type="text"
                   onKeyDown={e => dispatch({type: 'keyPress', value: e})} className={classes.join(' ')}
                   maxLength={4}
                   value={stars || keyDownStars}
                   onChange={() => ''}
            />
            <p>{accessGranted || accessDenied}</p>
            <form className="Buttons-Container">
                <div>
                    <input value="7" onClick={e => numbers(e.target.value)} type="button"/>
                    <input value="8" onClick={e => numbers(e.target.value)} type="button"/>
                    <input value="9" onClick={e => numbers(e.target.value)} type="button"/>
                </div>
                <div>
                    <input value="6" onClick={e => numbers(e.target.value)} type="button"/>
                    <input value="5" onClick={e => numbers(e.target.value)} type="button"/>
                    <input value="4" onClick={e => numbers(e.target.value)} type="button"/>
                </div>
                <div>
                    <input value="3" onClick={e => numbers(e.target.value)} type="button"/>
                    <input value="2" onClick={e => numbers(e.target.value)} type="button"/>
                    <input value="1" onClick={e => numbers(e.target.value)} type="button"/>
                </div>
                <div>
                    <input value="<" onClick={deleteLastSymbol} type="button"/>
                    <input value="0" onClick={e => numbers(e.target.value)} type="button"/>
                    <input value="E" onClick={checkPassword} type="button"/>
                </div>
            </form>
        </div>
    );
};

export default Simulator;